import Vue from 'vue'
import Vuex from 'vuex'
import network from "../network/index"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    yi: [],
    sousuo: ["女士牛仔裤", "针织衫", "衬衫", "皮衣", "风衣", "女士包包", "女士护肤品", "男士牛仔裤", "男用剃须刀", "男款体恤", "男士西装", "童装", "童袜", "童鞋", ],
    lishi: [],
    history: []
  },
  mutations: {
    puls1(state, item) {
      state.yi = [...item]
    },
    puls2(state, kw) {
      state.lishi = []
      state.sousuo.map(element => {
        if (element.includes(kw)) {
          state.lishi.push(element);
        }
      })
    },
    puls3(state, kw) {
       state.history.push(kw)
      // var index = state.history.findIndex((element) => {
      //   return element === kw
      // });

      // if (index !== -1) {
      //   state.history.splice(index, 1);
      // }
      // state.history.unshift(kw);

      // if (state.history.length > 4) {
      //   state.history.pop();
      // }
    },
    puls4(state) {
      state.history = []
    }
  },
  actions: {
    puls1(context) {
      network
        .getStoreList11()
        .then(res => {
          console.log(res.data.data);
          context.commit('puls1', res.data.data)
        })
        .catch(err => {
          // console.error(err);
        });
    },
    puls2(context, kw) {
      context.commit('puls2', kw)
    },
    puls3(context, kw) {
      context.commit('puls3', kw)
    },
    puls4(context) {
      context.commit('puls4')
    }

  },
  modules: {}
})