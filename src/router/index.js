import Vue from 'vue'
import VueRouter from 'vue-router'
import homeModule from "./homeModule"

Vue.use(VueRouter)
const routes = [
  {
    path:"/",
    name:"Main",
    redirect:'/Sou',
    component:() => import('../views/Main.vue'),
    children:[...homeModule]
  } ,{
    path:"/Tu",
    name:"Tu",
    component:() => import('../components/Tu.vue')
  },
  {
    path:"/Den",
    name:"Den",
    component:() => import('../views/Den.vue')
  },
  {
    path:"/Zuce",
    name:"Zuce",
    component:() => import('../views/Zuce.vue')
  },
  {
    path:"/Sp",
    name:"Sp",
    component:() => import('../components/Sp.vue')
  },
  {
    path:"/List",
    name:"List",
    component:() => import('../views/List.vue')
  },
  {
    path:"/Ti",
    name:"Ti",
    component:() => import('../components/Ti.vue')
  },
  {
    path:"/Yan",
    name:"Yan",
    component:() => import('../views/Yan.vue')
  },
  {
    path:"/Xq",
    name:"Xq",
    component:() => import('../views/Xq.vue')
  },
  {
    path:"/Tui",
    name:"Tui",
    component:() => import('../views/Tui.vue')
  },
  {
    path:"/Ren",
    name:"Ren",
    component:() => import('../components/Ren.vue')
  },
  {
    path:"/Spxq",
    name:"Spxq",
    component:() => import('../views/Spxq.vue')
  },
  {
    path:"/Jifen",
    name:"Jifen",
    component:() => import('../views/Jifen.vue')
  },
  {
    path:"/Lijuan",
    name:"Lijuan",
    component:() => import('../views/Lijuan.vue')
  },
  {
    path:"/Sousuo",
    name:"Sousuo",
    component:() => import('../views/Sousuo.vue')
  }
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
