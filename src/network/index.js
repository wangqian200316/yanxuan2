import {request} from "./core"
import {GET,path, POST} from "./config"

const network = {
  getStoreList(params) {return request(GET,path.list,params)},
   getStoreList1(params){return request(POST,path.list1,params)},
   getStoreList2(params){return request(POST,path.list2,params)},
   getStoreList3(params){return request(POST,path.list3,params)},
   getStoreList4(params){return request(POST,path.list4,params)},
   getStoreList5(params) {return request(GET,path.list5,params)},
   getStoreList6(params) {return request(GET,path.list6,params)},
   getStoreList7(params) {return request(GET,path.list7,params)},
   getStoreList8(params) {return request(GET,path.list8,params)},
   getStoreList9(params) {return request(GET,path.list9,params)},
   getStoreList10(params){return request(POST,path.list10,params)},
   getStoreList11(params){return request(POST,path.list11,params)}
}
export default network;


