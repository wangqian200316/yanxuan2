import {GET,POST} from "./config"
import axios from "axios";



const instance = axios.create({
  baseURL:"https://api.it120.cc",//发送请求时，会在url前拼接baseUrl
  timeout:10000,
  //设置axios为form-data
  headers:{'Content-Type':'application/x-www-form-urlencoded'},

  transformRequest:[function (data) {
   let ret = ''
   for (let it in data) {
     ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
   }
   return ret
 }]
})


export function request(method,url,params){
  switch (method){
    case GET:
      return get(url,params)
    case POST:
      return post(url,params)
  }
}

function get(url,params){
  return instance.get(url,params)
}

function post(url,params){
  return instance.post(url,params)
}



