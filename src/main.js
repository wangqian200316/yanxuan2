import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import network from "./network/index";
import routerConfig from './router/config';
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);
Vue.prototype.$network = network;
Vue.prototype.$routerConfig = routerConfig;
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
