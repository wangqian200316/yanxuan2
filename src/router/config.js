
import Home from '../views/Sou.vue'

const router = {
  Sou:{
    path: '/Sou',
    name: 'Sou',
    component: Home
  },
  Fen:{
    path:"/Fen",
    name:"Fen",
    component:() => import('../views/Fen.vue')
  },
  Cart:{
    path:"/Cart",
    name:"Cart",
    component:() => import('../views/Cart.vue')
  },
  Wode:{
    path:"/Wode",
    name:"Wode",
    component:() => import('../views/Wode.vue')
  },
  
 
}

export default router;